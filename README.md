# Git Command Line Tutorial #

This tutorial is quick and dirty. For best results read the manpage for each command and [this](http://git-scm.com/doc) site is also very helpful.

### Git Bash Shell - Not Windows Command Prompt ###
###### Technically you can use Windows cmd and cd to bash shell but it's ugly and painful.
* Open Git Bash from your Start menu.  `Start > All Programs > Git > Git Bash`
* Make sure you are in your home directory for your repositories.  If you don't know, refer back to the setup page.
* Your home dir should be something like `/c/dev` or `/c/dev/git` or whatever you setup initially.

### Cloning a Repository ###

* In Bitbucket (website) find your repository and click `clone` on the left bar.  It should look something like
> `git clone git@bitbucket.org:{owner}/{repository}`
* Let's pretend we have a repository in the RS_IT team named aul_programs. The clone statement would look like this:
> `git clone git@bitbucket.org:RS_IT/aul_progams.git`
* If you had your own repository not in the team repositories the owner portion would be your username for bitbucket.
>`git clone git@bitbucket.org:b_bob/billy_bobs_progams.git`
#### You now have the entire repository and all of it's branches and code on your machine.
* Change your directory to the location of the repository.
`cd /aul_programs`
* Your shell prompt will change from your `username@machine ~` to `username@machine ~repositoryname (branchname)`
* The branch name defaults to your main branch on the first run.  By default that is **master** but it could be **funct** or something else.

### Git Status and Git Log ###

These commands will give you a way to track and check your work

**Git log**

* Running `git log` shows a listing of recent commits.  The most recent is at the top.

**Git status** 

* Running `git status` will show you which branch you are currently "on" and whether or not you have uncommited changes.  It is a good idea to run this between other commands to get an idea of where you are in your work.
* For example, I just started work for the day and I can't remember whether or not my working directory is clean from yesterday. I run `git status` and it will show uncommitted changes or say the working directory is clean

### Add/Modify Files and Code ###

* At this point do the real work you were hired to do.  Program, script, and modify stuff that was cloned to your machine.
* When you come to a point where you want to use git to do snapshots of your code, perform a `git add` from the shell prompt.
* This is the same thing as *staging* in SourceTree.  It makes Git aware of the items you want git to be aware of.  Otherwise they are just files on your machine.
* Add everything in the working directory using a wildcard character `*` or add specific files by calling them out specifically after the `git add` command.
* EXAMPLE:  `git add *` adds everything in the repository that has changed. `git add Doom.exe` adds only the changed Doom executable file.

### Commit Changes to GIT ###

* Commits are like a stamp for a change.  They contain your name, email address, date and time, and a commit ID in the form of a hash value.
* Adding a message on the commit is important to identify what is in the commit or what you changed and of what you wanted a snapshot in time .
*Syntax for commit is `git commit -m 'message'` where the `-m` is the option to add a message or comment to the commit.  The message follows the `-m` option.
* EXAMPLE: `git commit -m 'added doom.exe and changed leisure_suit_larry config files'`

### Changing Branches or Creating New Local Branch ###

* To change to another branch (assuming it is already in existence): 
> `git checkout {branchname}` 
>
> `git checkout release`
* To create a new branch and change to it:
> `git checkout -b {branchname}`
>
> `git checkout -b wms12345`


### Push Changes to Remote Repository (Bitbucket) ###

* The big question about pushing changes is when to do so.  It is all about preference.  Do as you like.
* The syntax is `git push {option} {repository}`
* To understand the option you have to understand _remote_ and _origin_. 
* Look at [ this ](http://git-scm.com/book/en/v2/Git-Basics-Working-with-Remotes) for an explanation.
* EXAMPLE: `git push origin master` or `git push origin funct` to push local changes to `funct` branch to remote `funct` branch.

### Pull Change from Remote Repository (Bitbucket) ###

* The basic syntax for `git pull` is similar to `git push`
> `git pull {option} {branch}
* To pull changes in the _funct_ branch in Bitbucket to your _funct_ branch locally.
> `git pull origin funct`
